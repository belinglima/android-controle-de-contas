package com.example.belinglima.appcontroledecontasareceber;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ClienteDB extends SQLiteOpenHelper {

    public ClienteDB(Context context) {

        super(context, "boteco.sqlite", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table if not exists devedores(\n" +
                "_id integer primary key autoincrement,\n" +
                "nome text, apelidoIntimo text, dataVenda text, valor real, descricao text, piadaPosPagamento text);";
        db.execSQL(sql);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long incluiCliente(Cliente cliente){
        SQLiteDatabase db = getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put("nome", cliente.getNome());
            values.put("apelidoIntimo", cliente.getApelidoIntimo());
            values.put("dataVenda", cliente.getDataVenda());
            values.put("valor", cliente.getValor());
            values.put("descricao", cliente.getDescrcao());
            values.put("piadaPosPagamento", cliente.getPiadaPosPagamento());
            long id = db.insert("devedores", "", values);
            return  id;

        } finally {
            db.close();
        }
    }

    public Cliente buscaCliente(int id){
        SQLiteDatabase db = getReadableDatabase();
        try {
            Cursor c = db.query("devedores", null,
                    "_id=?",
                     new String[]{String.valueOf(id)},
                    null, null, null);
            if (c.getCount() > 0){
                c.moveToFirst();
                //getstring numero da coluna da tabela
                String nome = c.getString(1);
                String apelidoIntimo = c.getString(2);
                String dataVenda = c.getString(3);
                double valor = c.getDouble(4);
                String descricao = c.getString(5);
                String piadaPosPagamento = c.getString(6);
                return new Cliente(id, nome, apelidoIntimo, dataVenda, valor, descricao, piadaPosPagamento);
            }else {
                return new Cliente(0,"", "", "", 0, "", "");
            }
        }finally {
            db.close();
        }
    }

    public long alteraCliente(Cliente cliente){
        SQLiteDatabase db = getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put("nome", cliente.getNome());
            values.put("apelidoIntimo", cliente.getApelidoIntimo());
            values.put("dataVenda", cliente.getDataVenda());
            values.put("valor", cliente.getValor());
            values.put("descricao", cliente.getDescrcao());
            values.put("piadaPosPagamento", cliente.getPiadaPosPagamento());
            long id = db.update("devedores", values,
                    "_id=?",
                    new String[]{String.valueOf(cliente.getId())});
            return  id;

        } finally {
            db.close();
        }
    }

    public long excluiCliente(int id){
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.delete("devedores",
                    "_id=?",
                    new String[]{String.valueOf(id)});
            return  id;

        } finally {
            db.close();
        }
    }


    public Cursor listaFiado() {
        Cursor cursor;
        String campos[] = {"_id", "nome", "apelidoIntimo", "dataVenda", "valor", "descricao", "piadaPosPagamento"};
        SQLiteDatabase db = getReadableDatabase();
        cursor = db.query("devedores", campos, null, null,
                null, null, null, null);
        if (cursor!=null) {
            cursor.moveToFirst();
        }
        db.close();
        return cursor;
    }
    public Cursor pesqDevedor(String palavra) {
        Cursor cursor;
        String campos[] = {"_id", "nome", "apelidoIntimo", "dataVenda", "valor", "descricao", "piadaPosPagamento"};
        SQLiteDatabase db = getReadableDatabase();
        cursor = db.query("devedores", campos, "nome LIKE ?", new
                        String[]{"%"+palavra+"%"},
                null, null, null, null);
        if (cursor!=null) {
            cursor.moveToFirst();
        }
        db.close();
        return cursor;
    }

}
