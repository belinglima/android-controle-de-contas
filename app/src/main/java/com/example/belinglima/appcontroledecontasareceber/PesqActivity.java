package com.example.belinglima.appcontroledecontasareceber;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class PesqActivity extends AppCompatActivity {

    private EditText edtPesq;
    private ListView listPesq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pesq);

        edtPesq = findViewById(R.id.edtPesq);
        listPesq = findViewById(R.id.listPesq);
    }
    public void pesquisar(View view) {
        String pesq = edtPesq.getText().toString();
        if (pesq == "") {
            Toast.makeText(this, "Informe o nome do bebum",
                    Toast.LENGTH_LONG).show();
            edtPesq.requestFocus();
            return;
        }
        ClienteDB ClienteDB = new ClienteDB(this);
        Cursor cursor = ClienteDB.pesqDevedor(pesq);

        String[] nomeCampos = new String[]{"_id", "nome", "apelidoIntimo", "dataVenda", "valor", "descricao", "piadaPosPagamento"};

        int[] idViews = new int[] {R.id.txtId, R.id.txtNome, R.id.txtApelido, R.id.txtData, R.id.txtValor, R.id.txtDescricao, R.id.txtPiada};

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                R.layout.item_boteco, cursor, nomeCampos,
                idViews, 0);

        listPesq.setAdapter(adapter);
    }
}
