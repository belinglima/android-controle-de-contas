package com.example.belinglima.appcontroledecontasareceber;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class ListaActivity extends AppCompatActivity {

    private ListView listFiados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        listFiados = findViewById(R.id.listFiados);

        ClienteDB ClienteDB = new ClienteDB(this);
        Cursor cursor = ClienteDB.listaFiado();


        String[] nomeCampos = new String[]{"_id", "nome", "apelidoIntimo", "dataVenda", "valor", "descricao", "piadaPosPagamento"};
        int[] idViews = new int[] {R.id.txtId, R.id.txtNome, R.id.txtApelido, R.id.txtData, R.id.txtValor, R.id.txtDescricao, R.id.txtPiada};

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                R.layout.item_boteco, cursor, nomeCampos,
                idViews, 0);

        listFiados.setAdapter(adapter);
    }
}
