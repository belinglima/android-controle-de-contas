package com.example.belinglima.appcontroledecontasareceber;

public class Cliente {

    private int id;
    private String nome;
    private String apelidoIntimo;
    private String dataVenda;
    private double valor;
    private String descricao;
    private String piadaPosPagamento;

    public Cliente(int id, String nome, String apelidoIntimo, String dataVenda, double valor, String descricao, String piadaPosPagamento) {
        this.id = id;
        this.nome = nome;
        this.apelidoIntimo = apelidoIntimo;
        this.dataVenda = dataVenda;
        this.valor = valor;
        this.descricao = descricao;
        this.piadaPosPagamento = piadaPosPagamento;
    }

    public int getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getApelidoIntimo() {
        return apelidoIntimo;
    }

    public String getDataVenda() {
        return dataVenda;
    }

    public double getValor() {
        return valor;
    }

    public String getDescrcao() {
        return descricao;
    }

    public String getPiadaPosPagamento() {
        return piadaPosPagamento;
    }

}
