package com.example.belinglima.appcontroledecontasareceber;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;



public class MainActivity extends AppCompatActivity {

    private EditText edtId;
    private EditText edtNome;
    private EditText edtApelidoIntimo;
    private EditText edtDataVenda;
    private EditText edtValor;
    private EditText edtDescricao;
    private EditText edtPiadaPosPagamento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtId = findViewById(R.id.edtId);
        edtNome = findViewById(R.id.edtNome);
        edtApelidoIntimo = findViewById(R.id.edtApelidoIntimo);
        edtDataVenda = findViewById(R.id.edtDataVenda);
        edtValor = findViewById(R.id.edtValor);
        edtDescricao = findViewById(R.id.edtDescricao);
        edtPiadaPosPagamento = findViewById(R.id.edtPiadaPosPagamento);


    }

    public void consultar(View view) {
        String idS = edtId.getText().toString();

        if (idS.trim().isEmpty()){
            Toast.makeText(this, "Informe o codigo do bebum",
                    Toast.LENGTH_LONG).show();
            edtId.requestFocus();
            return;
        }

        int id = Integer.parseInt(idS);

        ClienteDB clienteDB = new ClienteDB(this);

        Cliente cliente = clienteDB.buscaCliente(id);

        if (cliente.getId() == 0){
            Toast.makeText(this, "bebum não cadastrado com este codigo",
                    Toast.LENGTH_LONG).show();
        }
        edtNome.setText(cliente.getNome());
        edtApelidoIntimo.setText(String.valueOf(cliente.getApelidoIntimo()));
        edtDataVenda.setText(String.valueOf(cliente.getDataVenda()));
        edtValor.setText(String.valueOf(cliente.getValor()));
        edtDescricao.setText(String.valueOf(cliente.getDescrcao()));
        edtPiadaPosPagamento.setText(String.valueOf(cliente.getPiadaPosPagamento()));
        edtId.requestFocus();



    }

    public void incluir(View view) {
        String nome = edtNome.getText().toString();
        String apelidointimo = edtApelidoIntimo.getText().toString();
        String datavenda = edtDataVenda.getText().toString();
        String valor = edtValor.getText().toString();
        String descricao = edtDescricao.getText().toString();
        String piadapospagemento = edtPiadaPosPagamento.getText().toString();

        if (nome.trim().isEmpty() || apelidointimo.trim().isEmpty() || datavenda.trim().isEmpty()
                || valor.trim().isEmpty() || descricao.trim().isEmpty() || piadapospagemento.trim().isEmpty()){
            Toast.makeText(this, "Informe os dados carretamente",
                    Toast.LENGTH_LONG).show();
            edtNome.requestFocus();
            return;
        }

        double valorD = Double.parseDouble(valor);

        ClienteDB clienteDB = new ClienteDB(this);

        long id = clienteDB.incluiCliente(new Cliente(0, nome, apelidointimo, datavenda, valorD, descricao, piadapospagemento));

        if (id > 0){
            Toast.makeText(this,
                    "Ok! Mais um bebado cadastrado com codigo: " + id,
                    Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Erro ..... não cadastrado",
                    Toast.LENGTH_LONG).show();
        }

        edtId.setText("");
        edtNome.setText("");
        edtApelidoIntimo.setText("");
        edtDataVenda.setText("");
        edtValor.setText("");
        edtDescricao.setText("");
        edtPiadaPosPagamento.setText("");
        edtNome.requestFocus();


    }

    public void alterar(View view) {
        String nome = edtNome.getText().toString();
        String apelidointimo = edtApelidoIntimo.getText().toString();
        String datavenda = edtDataVenda.getText().toString();
        String valor = edtValor.getText().toString();
        String descricao = edtDescricao.getText().toString();
        String piadapospagemento = edtPiadaPosPagamento.getText().toString();
        String id = edtId.getText().toString();

        if (id.trim().isEmpty() || nome.trim().isEmpty() || apelidointimo.trim().isEmpty() || datavenda.trim().isEmpty()
                || valor.trim().isEmpty() || descricao.trim().isEmpty() || piadapospagemento.trim().isEmpty()){
            Toast.makeText(this, "Informe os dados Corretos",
                    Toast.LENGTH_LONG).show();
            edtNome.requestFocus();
            return;
        }
        int idD = Integer.parseInt(id);
        double valorD = Double.parseDouble(valor);

        ClienteDB clienteDB = new ClienteDB(this);


        if (clienteDB.alteraCliente(new Cliente(idD, nome, apelidointimo, datavenda, valorD, descricao, piadapospagemento)) > 0){
            Toast.makeText(this,
                    "Ok! Cliente alterado com sucesso",
                    Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Erro ..... não alterado",
                    Toast.LENGTH_LONG).show();
        }

        edtId.setText("");
        edtNome.setText("");
        edtApelidoIntimo.setText("");
        edtDataVenda.setText("");
        edtValor.setText("");
        edtDescricao.setText("");
        edtPiadaPosPagamento.setText("");
        edtNome.requestFocus();

    }

    public void excluir(View view) {
        String nome = edtNome.getText().toString();
        String apelidointimo = edtApelidoIntimo.getText().toString();
        String datavenda = edtDataVenda.getText().toString();
        String valor = edtValor.getText().toString();
        String descricao = edtDescricao.getText().toString();
        String piadapospagemento = edtPiadaPosPagamento.getText().toString();
        String id = edtId.getText().toString();

        if (id.trim().isEmpty() || nome.trim().isEmpty() || apelidointimo.trim().isEmpty() || datavenda.trim().isEmpty()
                || valor.trim().isEmpty() || descricao.trim().isEmpty() || piadapospagemento.trim().isEmpty()){
            Toast.makeText(this, "Informe os dados do freguês",
                    Toast.LENGTH_LONG).show();
            edtNome.requestFocus();
            return;
        }
        int idD = Integer.parseInt(id);

        ClienteDB clienteDB = new ClienteDB(this);


        if (clienteDB.excluiCliente(idD) > 0){
            Toast.makeText(this,
                    "Ok! bebum quitou a conta.",
                    Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Erro ... ainda não foi excluido ..",
                    Toast.LENGTH_LONG).show();
        }

        edtId.setText("");
        edtNome.setText("");
        edtApelidoIntimo.setText("");
        edtDataVenda.setText("");
        edtValor.setText("");
        edtDescricao.setText("");
        edtPiadaPosPagamento.setText("");
        edtNome.requestFocus();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        ((MenuInflater) inflater).inflate(R.menu.cad_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.mnListagem) {
            Intent it = new Intent(this, ListaActivity.class);
            startActivity(it);
        } else if (item.getItemId() == R.id.mnPesquisa) {
            Intent it = new Intent(this, PesqActivity.class);
            startActivity(it);
        }
        return true;
    }
}
